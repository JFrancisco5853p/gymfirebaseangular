import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public title: string = 'mastergym';
  public Usuarios: any;

  constructor() {
    this.Usuarios = '';
  }

  ngOnInit(): void {
    this.Usuarios = localStorage.getItem('User') ? (JSON.parse(localStorage.getItem('User') as string)).user: '';
  }

  recibiRespuesta(evento:any){
    this.Usuarios = evento.user;
  }
}
