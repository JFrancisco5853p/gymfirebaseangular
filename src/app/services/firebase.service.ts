import { Injectable } from '@angular/core';
import { initializeApp } from 'firebase/app'
import { getAuth } from 'firebase/auth';
import { getFirestore, collection, getDocs, doc, setDoc, getDoc, updateDoc } from 'firebase/firestore';
import { getStorage } from 'firebase/storage';

const firebaseConfig = {

};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getFirestore(app);
const storage = getStorage(app);

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  public firebApp: any;
  public firebAuth: any;
  public firebDB: any;
  public fireStorage: any;

  constructor() {
    this.firebApp = app;
    this.firebAuth = auth;
    this.firebDB = db;
    this.fireStorage = storage;
  };

  async registrarDocuments(NomDoc: string, data:any){
    const newClientRef = doc(collection(db, NomDoc));
    return await setDoc(newClientRef, data)
  };

  async obtenerRegistrosDocuments(NomDoc: string){
    return await getDocs(collection(db, NomDoc));
  };

  async obtenerDocumento(NomDoc:string, id:any){
    const docRef = doc(db, NomDoc, id);
    return await getDoc(docRef);
  };

  async obtenerDocumentReferenc(data: any){
    return await getDoc(data);
  };

  async actualizarDocuments(NomDoc: string, id:any, newData: any){
    const clientId = doc(db, NomDoc, id);
    return await updateDoc(clientId, newData);
  }

}
