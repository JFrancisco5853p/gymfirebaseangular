import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Cliente } from 'src/app/models/cliente';
import { FirebaseService } from 'src/app/services/firebase.service';
import { MensajesService } from 'src/app/services/mensajes.service';

@Component({
  selector: 'app-seleccionar-cliente',
  templateUrl: './seleccionar-cliente.component.html',
  styleUrls: ['./seleccionar-cliente.component.css']
})
export class SeleccionarClienteComponent implements OnInit {
  public clientes: Cliente[];
  @Input('nombre') nombre: any;
  @Output('seleccionoCliente') seleccionoCliente = new EventEmitter();
  @Output('canceloCliente') canceloCliente = new EventEmitter();

  constructor(private fbService: FirebaseService, private msj: MensajesService) {
    this.clientes = new Array<Cliente>();
    this.nombre = '';
  };

  ngOnInit(): void {
    this.clientes.length = 0;
    this.fbService.obtenerRegistrosDocuments('clientes')
      .then((result) => {
        result.docs.forEach((item) => {
          let cliente = item.data();
          cliente['id'] = item.id;
          cliente['ref'] = item.ref;
          cliente['visible'] = false;
          this.clientes.push(cliente as Cliente);
        });
      })
      .catch((error) => {
        this.msj.mensajeError('Error', error.message);
      });
  };

  buscarCliente(nombre: any) {
    this.clientes.forEach((cliente) => {
      if (cliente.nombre.toLowerCase().includes(nombre.value.toLowerCase())) 
      {
        cliente.visible = true;
      }
      else
      {
        cliente.visible = false;
      }
    });
  };

  seleccionarCliente(cliente: Cliente) {
    this.nombre = `${cliente.nombre} ${cliente.apellido}`;
    this.clientes.forEach((cliente)=>{
      cliente.visible = false;
    });
    this.seleccionoCliente.emit(cliente);
  };

  cancelarCliente() {
    this.nombre = undefined;
    this.canceloCliente.emit();
  };
};