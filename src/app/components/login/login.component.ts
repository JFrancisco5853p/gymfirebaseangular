import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { signInWithEmailAndPassword, UserCredential } from "firebase/auth";
import { NgxSpinnerService } from 'ngx-spinner';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public formularioLogin: FormGroup;
  public datosCorrectos: boolean;
  public textoError: string;
  @Output() RUsuarios: EventEmitter<UserCredential>;

  constructor(public creadorFormulario: FormBuilder, private spinner: NgxSpinnerService, private fbService: FirebaseService) {
    /*Creación de formulario*/
    this.formularioLogin = this.creadorFormulario.group({
      email: ['', Validators.compose([
        Validators.required, Validators.email
      ])],
      password: ['', Validators.required]
    });
    /*Emitir valores al padre*/
    this.RUsuarios = new EventEmitter<UserCredential>; 
    /*Variable para mostrar u ocultar mensaje, cuando el usuario no ingrese los valores correctos*/
    this.datosCorrectos = true;
    this.textoError = '';
  };

  ngOnInit():void
  {

  };

  Ingresar(): void
  {
    if (this.formularioLogin.valid)
    {
      this.datosCorrectos = true;
      this.spinner.show();
      setTimeout(() => {
        signInWithEmailAndPassword(this.fbService.firebAuth, this.formularioLogin.value.email, this.formularioLogin.value.password)
        .then((Usuario) => {
          /*Asignamos el usuario al emisor*/
          this.RUsuarios.emit(Usuario);
          localStorage.setItem('User', JSON.stringify(Usuario));
          this.spinner.hide();
        })
        .catch((error) => {
          if(error.code == "auth/user-not-found")
          {
            this.datosCorrectos = false;
            this.textoError = "El usuario no existe"
          }
          else if(error.code == "auth/wrong-password")
          {
            this.datosCorrectos = false;
            this.textoError = "Contraseña incorrecta"
          }
          else
          {
            this.datosCorrectos = false;
            this.textoError = "Error. No puede logear en estos momentos."
          }
          this.spinner.hide();
        });
      }, 1000);
    } 
    else 
    {
      this.datosCorrectos = false;
      this.textoError = 'Por favor revisa que los datos esten correctos...';
    }
  };

}
