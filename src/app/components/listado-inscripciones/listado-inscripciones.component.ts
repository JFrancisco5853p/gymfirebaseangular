import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Inscripciones } from 'src/app/models/inscripciones';
import { FirebaseService } from 'src/app/services/firebase.service';
import { MensajesService } from 'src/app/services/mensajes.service';

@Component({
  selector: 'app-listado-inscripciones',
  templateUrl: './listado-inscripciones.component.html',
  styleUrls: ['./listado-inscripciones.component.css']
})
export class ListadoInscripcionesComponent implements OnInit {
  public inscripciones: Inscripciones[];
  constructor(
    private fbService: FirebaseService, 
    private msj: MensajesService,
    private spinner: NgxSpinnerService){
    this.inscripciones = [];
  };

  ngOnInit(): void {
    this.spinner.show();
    this.inscripciones.length = 0;
    this.fbService.obtenerRegistrosDocuments('inscripciones')
      .then((resultado) => {
        resultado.forEach((resultado) => {
          let inscripcionObtenida = resultado.data() as any;
          inscripcionObtenida.id = resultado.id;
          this.fbService.obtenerDocumentReferenc(inscripcionObtenida.cliente)
            .then((cliente) => {
              inscripcionObtenida.clienteObtenido = cliente.data();
              inscripcionObtenida.fecha = new Date(inscripcionObtenida.fecha.seconds * 1000);
              inscripcionObtenida.fechaFinal = new Date(inscripcionObtenida.fechaFinal.seconds * 1000);
              this.inscripciones.push(inscripcionObtenida);
              this.spinner.hide();
            })
            .catch((error) => {
              this.msj.mensajeError('Error', error.message);
            });
        });
      })
      .catch((error) => {
        this.msj.mensajeError('Error', error.message);
      });
  };
}
