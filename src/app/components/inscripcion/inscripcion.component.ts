import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/models/cliente';
import { Inscripciones } from 'src/app/models/inscripciones';
import { Precio } from 'src/app/models/precios';
import { FirebaseService } from 'src/app/services/firebase.service';
import { MensajesService } from 'src/app/services/mensajes.service';

@Component({
  selector: 'app-inscripcion',
  templateUrl: './inscripcion.component.html',
  styleUrls: ['./inscripcion.component.css']
})
export class InscripcionComponent implements OnInit{
  public inscripcion: Inscripciones;
  public clienteSeleccionado: Cliente;
  public precioSeleccionado: Precio;
  public precios: Precio[];
  public idPrecio: string;

  constructor(private fbService: FirebaseService, public msj: MensajesService)
  {
    this.inscripcion = new Inscripciones();
    this.clienteSeleccionado = new Cliente();
    this.precios = new Array<Precio>;
    this.precioSeleccionado = new Precio();
    this.idPrecio = 'null';
  };

  ngOnInit(): void {
      this.fbService.obtenerRegistrosDocuments('precios')
      .then((result)=>
      {
        result.docs.forEach((item)=>{
          let precios = item.data() as Precio;
          precios.id = item.id;
          precios.ref = item.ref;
          this.precios.push(precios);
        });
      }).catch(()=>
      {
        this.msj.mensajeError('Error', 'Error al obtener documento de precios');
      });
  }

  asignarCliente(cliente: Cliente)
  {
    this.inscripcion.cliente = cliente.ref;
    this.clienteSeleccionado = cliente;
  };

  eliminarCliente(){
    this.clienteSeleccionado = new Cliente();
    this.inscripcion.cliente = undefined as any;
  };

  guardar()
  {
    if (this.inscripcion.validar().esValido)
    {
      let inscripcionesAgregar = {
        fecha: this.inscripcion.fecha, 
        fechaFinal: this.inscripcion.fechaFinal,
        cliente: this.inscripcion.cliente,
        precios: this.inscripcion.precios,
        subTotal: this.inscripcion.subTotal,
        isv: this.inscripcion.isv,
        total: this.inscripcion.total
      }
      this.fbService.registrarDocuments('inscripciones', inscripcionesAgregar)
      .then((result) => {
        this.inscripcion = new Inscripciones();
        this.clienteSeleccionado = new Cliente();
        this.precioSeleccionado = new Precio();
        this.idPrecio = 'null';
        this.msj.mensajeCorrecto("Correcto", "El documento se dio de alta correctamente");
      })
      .catch((error) => {
        this.msj.mensajeError("Error", error.message);
      });
    } 
    else 
    {
      this.msj.mensajeAdvertencia('Advertencia', this.inscripcion.validar().mensaje);
    };
  };

  seleccionarPrecio(id: any) {
    if (id.value != "null"){
      this.precioSeleccionado = (this.precios.find((x) => (x.id === id.value)) as Precio);
      this.inscripcion.precios = this.precioSeleccionado.ref;
  
      this.inscripcion.subTotal = this.precioSeleccionado.costo;
      this.inscripcion.isv = this.inscripcion.subTotal * 0.15;
      this.inscripcion.total = this.inscripcion.subTotal + this.inscripcion.isv;
  
      this.inscripcion.fecha = new Date();
  
      if (this.precioSeleccionado.tipoDuracion == 1) {
        /* DIA */
        let dias: number = this.precioSeleccionado.duracion;
        let fechaFinal = new Date
          (
            this.inscripcion.fecha.getFullYear(),
            this.inscripcion.fecha.getMonth(),
            this.inscripcion.fecha.getDate() + dias
          );
        this.inscripcion.fechaFinal = fechaFinal;
      };
      if (this.precioSeleccionado.tipoDuracion == 2) {
        /* SEMANAL */
        let dias: number = this.precioSeleccionado.duracion * 7;
        let fechaFinal = new Date
          (
            this.inscripcion.fecha.getFullYear(),
            this.inscripcion.fecha.getMonth(),
            this.inscripcion.fecha.getDate() + dias
          );
        this.inscripcion.fechaFinal = fechaFinal;
      };
      if (this.precioSeleccionado.tipoDuracion == 3) {
        /* QUINCENAL */
        let dias: number = this.precioSeleccionado.duracion * 15;
        let fechaFinal = new Date
          (
            this.inscripcion.fecha.getFullYear(),
            this.inscripcion.fecha.getMonth(),
            this.inscripcion.fecha.getDate() + dias
          );
        this.inscripcion.fechaFinal = fechaFinal;
      };
      if (this.precioSeleccionado.tipoDuracion == 4) {
        /* MENSUAL */
        let anio: number = this.inscripcion.fecha.getFullYear();
        let meses = this.precioSeleccionado.duracion + this.inscripcion.fecha.getMonth();
        let dia: number = this.inscripcion.fecha.getDate();
  
        let fechaFinal = new Date (anio,meses,dia);
        this.inscripcion.fechaFinal = fechaFinal;
      };
      if (this.precioSeleccionado.tipoDuracion == 5) {
        /* ANUAL */
        let anio: number = this.inscripcion.fecha.getFullYear() + this.precioSeleccionado.duracion;
        let meses = this.inscripcion.fecha.getMonth();
        let dia: number = this.inscripcion.fecha.getDate();
  
        let fechaFinal = new Date (anio,meses,dia);
        this.inscripcion.fechaFinal = fechaFinal;
      };
    } else {
      this.precioSeleccionado = new Precio();
      this.inscripcion.fecha = null as any;
      this.inscripcion.fechaFinal = null as any;
      this.inscripcion.precios = null as any;
      this.inscripcion.subTotal = 0;
      this.inscripcion.isv = 0;
      this.inscripcion.total = 0;
    }
  };
};
