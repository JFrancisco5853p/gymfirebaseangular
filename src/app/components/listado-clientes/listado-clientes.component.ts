import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-listado-clientes',
  templateUrl: './listado-clientes.component.html',
  styleUrls: ['./listado-clientes.component.css']
})
export class ListadoClientesComponent implements OnInit {
  public Clientes: any[];
  constructor(private fbService: FirebaseService, private spinner: NgxSpinnerService){
    this.Clientes = new Array<any>();
  };

  ngOnInit(): void {
    this.spinner.show();
    this.fbService.obtenerRegistrosDocuments("clientes").then((datos) => {
      datos.forEach((doc) => {
        let cliente = doc.data();
        cliente['id'] = doc.id;
        cliente['ref'] = doc.ref;
        this.Clientes.push(cliente);
      });
      this.spinner.hide();
    });
  };

}
