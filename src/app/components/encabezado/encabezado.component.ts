import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { signOut } from "firebase/auth";
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-encabezado',
  templateUrl: './encabezado.component.html',
  styleUrls: ['./encabezado.component.css']
})
export class EncabezadoComponent implements OnInit {
  public UsuarioL: any;
  @Output() vcerrarSession: EventEmitter<boolean>;

  constructor(
    private fbService: FirebaseService,
    public routes: Router
  )
  {
    this.vcerrarSession = new EventEmitter;
  }

  ngOnInit(): void {
    
  }

  cerrarSession()
  {
    signOut(this.fbService.firebAuth);
    localStorage.removeItem('User');
    this.routes.navigate(['/']);
    this.vcerrarSession.emit(true);
  }

}
