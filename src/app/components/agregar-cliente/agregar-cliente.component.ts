import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { getDownloadURL, getStorage, ref, uploadBytesResumable } from "firebase/storage";
import { Cliente } from 'src/app/models/cliente';
import { FirebaseService } from 'src/app/services/firebase.service'; 
import { MensajesService } from 'src/app/services/mensajes.service';

@Component({
  selector: 'app-agregar-cliente',
  templateUrl: './agregar-cliente.component.html',
  styleUrls: ['./agregar-cliente.component.css']
})
export class AgregarClienteComponent implements OnInit{
  public formularioCliente: FormGroup;
  public cargaImg: number;
  public fireImgUrl: string;
  public modelCliente: Cliente;
  public esEditable: boolean;
  public id: string;
  
  constructor(
    private fb: FormBuilder,
    private fbService: FirebaseService,
    private activeRoute: ActivatedRoute,
    private msj: MensajesService
  ) {
    this.formularioCliente = new FormGroup({});
    this.cargaImg = 0;
    this.fireImgUrl = '';
    this.modelCliente = new Cliente();
    this.esEditable = false;
    this.id = '';
  };

  ngOnInit(): void {
    this.formularioCliente = this.fb.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      correo: ['', Validators.compose([
        Validators.required, Validators.email
      ])],
      cedula: [''],
      fechaNacimiento: ['', Validators.required],
      telefono: [''],
      imgUrl: ['', Validators.required]
    });

    this.id = this.activeRoute.snapshot.params['clienteID'];
    if (this.id !== undefined) {
      this.esEditable = true;
      this.fbService.obtenerDocumento("clientes", this.activeRoute.snapshot.params['clienteID']).then((doc) => {
        if (doc.exists()) {
          this.modelCliente = doc.data() as Cliente;
          this.formularioCliente.setValue({
            nombre: this.modelCliente.nombre,
            apellido: this.modelCliente.apellido,
            correo: this.modelCliente.correo,
            cedula: this.modelCliente.cedula,
            fechaNacimiento: new Date(doc.data()['fechaNacimiento'].seconds * 1000).toISOString().substring(0, 10),
            telefono: this.modelCliente.telefono,
            imgUrl: ''
          });
        } else {
          this.msj.mensajeError('Error', 'El documento no existe...');
        }
      });
    };
  };

  agregar(){
    if (this.cargaImg === 100){
      this.formularioCliente.value.imgUrl = this.fireImgUrl;
      this.formularioCliente.value.fechaNacimiento = new Date(this.formularioCliente.value.fechaNacimiento);
      this.fbService.registrarDocuments("clientes", this.formularioCliente.value)
      .then(() => {
        this.msj.mensajeCorrecto('Se registro correctamente', 'El registro se dio de alta correctamente');
      })
      .catch((error) => {
        this.msj.mensajeError('Error', error.message);
      });
    };
  };

  editar() {
    if (this.fireImgUrl == undefined || this.fireImgUrl == '') {
      this.msj.mensajeAdvertencia('Advertencia', 'Debe agregar una imagen')
    }
    else {
      this.formularioCliente.value.imgUrl = this.fireImgUrl;
      if (this.cargaImg === 100) {
        this.formularioCliente.value.imgUrl = this.fireImgUrl;
        this.formularioCliente.value.fechaNacimiento = new Date(this.formularioCliente.value.fechaNacimiento);
        this.fbService.actualizarDocuments("clientes", this.id, this.formularioCliente.value).then(() => {
          this.msj.mensajeCorrecto('Registro actualizado', 'El registro se ha actualizado correctamente');
        })
        .catch(()=>{
          this.msj.mensajeError('Error', 'Ocurrio un error al editar un registro');
        });
      };
    }
  };

  subirImg(evento:any){
    if (evento.target.files.length > 0) {
      let file = evento.target.files[0];
      let nomArchivo = new Date().getTime().toString();
      let extArchivo = file.name.toString().substring(file.name.toString().lastIndexOf('.'));
      const storage = getStorage();
      const storageRef = ref(storage, `clientes/${nomArchivo+extArchivo}`);
      const uploadTask = uploadBytesResumable(storageRef, file);
      uploadTask.on('state_changed', (snapshot) => {
        this.cargaImg = parseInt(((snapshot.bytesTransferred / snapshot.totalBytes) * 100).toString());
      },
        (error) => {
          this.msj.mensajeError('Error', error.message);
        },
        () => {
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            this.fireImgUrl = downloadURL;
          });
        });
    };
  };
}
