import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { Precio } from 'src/app/models/precios';
import { FirebaseService } from 'src/app/services/firebase.service';
import { MensajesService } from 'src/app/services/mensajes.service';

@Component({
  selector: 'app-precios',
  templateUrl: './precios.component.html',
  styleUrls: ['./precios.component.css']
})
export class PreciosComponent implements OnInit {
  public formularioPrecios: FormGroup;
  public preciosArray: any[];
  public esEditar: boolean;
  public id: string;

  constructor(
    private fb: FormBuilder, 
    private fbService: FirebaseService,
    private msj: MensajesService,
    private spinner: NgxSpinnerService
  )
  {
    this.formularioPrecios = new FormGroup({});
    this.preciosArray = new Array<any>();
    this.esEditar = false;
    this.id = '';
  };

  ngOnInit(): void {
    this.spinner.show();
    this.formularioPrecios = this.fb.group({
      nombre: ['', Validators.required],
      costo: ['', Validators.required],
      duracion: ['', Validators.required],
      tipoDuracion: ['', Validators.required]
    });
    this.mostrarPrecios(); 
  };

  mostrarPrecios()
  {
    this.fbService.obtenerRegistrosDocuments("precios")
    .then((datos)=>{
      this.preciosArray.length = 0;
      datos.forEach((doc) => {
        let precios = doc.data() as Precio;
        precios['id'] = doc.id;
        precios['ref'] = doc.ref;
        this.preciosArray.push(precios);
      });
      this.spinner.hide();
    })
    .catch((error)=> {
      this.msj.mensajeError('Error al obtener documentos de precios', error.message);
    });
  };

  agregar()
  {
    this.fbService.registrarDocuments("precios", this.formularioPrecios.value)
    .then(() => {
      this.msj.mensajeCorrecto('Correcto', 'El registro se dio de alta correctamente');
      this.formularioPrecios.reset();
      this.mostrarPrecios();
    })
    .catch((error)=> {
      this.msj.mensajeError('Error', error.message);
    });
  };

  editar(){
    this.fbService.actualizarDocuments("precios", this.id, this.formularioPrecios.value)
    .then(()=>{
      this.msj.mensajeCorrecto('Correcto', 'Se edito correctamente');
      this.formularioPrecios.reset();
      this.esEditar = false;
      this.mostrarPrecios();
    })
    .catch((error)=>{
      this.msj.mensajeError('Error', error.message);
    });
  };

  editarPrecio(precio: Precio){
    this.esEditar = true;
    this.id = precio.id;
    this.formularioPrecios.setValue({
      nombre: precio.nombre,
      costo: precio.costo,
      duracion: precio.duracion,
      tipoDuracion: precio.tipoDuracion
    });
  };

}
