export class Precio{
    public id:string;
    public nombre: string;
    public costo: number;
    public duracion: number;
    public tipoDuracion: number;
    public ref: any;

    constructor()
    {
        this.id = '';
        this.nombre = '';
        this.costo = -1;
        this.duracion = -1;
        this.tipoDuracion = -1;
        this.ref = '';
    }
}