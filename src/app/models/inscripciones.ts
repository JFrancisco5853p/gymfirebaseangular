import { DocumentReference } from "firebase/firestore";
import { Cliente } from "./cliente";

export class Inscripciones {
    public fecha: Date;
    public fechaFinal: Date;
    public subTotal: number;
    public isv: number;
    public total: number;

    public cliente = DocumentReference;
    public precios = DocumentReference;

    public id: string;
    public clienteObtenido: Cliente;

    constructor() 
    {
        this.fecha = undefined as any;
        this.fechaFinal = undefined as any;
        this.subTotal = '' as any;
        this.isv = '' as any;
        this.total = '' as any;
        this.cliente = undefined as any;
        this.precios = undefined as any;
        this.id = '';
        this.clienteObtenido = new Cliente();
    };

    validar(): any {
        let respuesta = 
        {
            esValido: false,
            mensaje: ''
        };
        if (this.cliente == null || this.cliente == undefined) 
        {
            respuesta.esValido = false;
            respuesta.mensaje = "Por favor seleccione un cliente";
            return respuesta;
        };
        if (this.precios == null || this.precios == undefined) 
        {
            respuesta.esValido = false;
            respuesta.mensaje = "No se ha seleccionado un precio";
            return respuesta;
        };
        if (this.fecha == null || this.fecha == undefined) 
        {
            respuesta.esValido = false;
            respuesta.mensaje = "No tiene fecha de inicio";
            return respuesta;
        };
        if (this.fechaFinal == null || this.fechaFinal == undefined) 
        {
            respuesta.esValido = false;
            respuesta.mensaje = "No tiene fecha final";
            return respuesta;
        };
        if (this.subTotal <= 0 || this.subTotal == undefined) 
        {
            respuesta.esValido = false;
            respuesta.mensaje = "No se ha podido calcular el subtotal";
            return respuesta;
        };
        if (this.isv <= 0 || this.isv == undefined) 
        {
            respuesta.esValido = false;
            respuesta.mensaje = "No se ha podido calcular el isv";
            return respuesta;
        };
        if (this.total <= 0 || this.total == undefined) 
        {
            respuesta.esValido = false;
            respuesta.mensaje = "No se ha podido calcular el total";
            return respuesta;
        };
        respuesta.esValido = true;
        return respuesta;
    };
};