export class Cliente{
    public id: string;
    public nombre: string;
    public apellido: string;
    public correo: string;
    public cedula: string;
    public fechaNacimiento: Date;
    public telefono: number;
    public imgUrl: string;
    public ref: any;
    public visible: boolean;

    constructor()
    {
        this.id = '';
        this.nombre= '';
        this.apellido= '';
        this.correo= '';
        this.cedula= '';
        this.fechaNacimiento= new Date();
        this.telefono= 0;
        this.imgUrl= '';
        this.ref = '';
        this.visible = false;
    }
}