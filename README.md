# gymFirebaseAngular

Application Gym

## Getting started

The project has the objective of managing a gym, to keep track of those who register, its functionality is to generate registration costs as well as show the start and end date.

Too, you can create new inscripcions for you clients. Too you can to create new registers for one suscripcions. 
To begin installing the project on your desktop, do the following: [Install project](#installation)!

## Name
Application to manage gym.

## Description
The application is prepared to manage a gym of a friend or someone you know, you can create prices based on the rate charged in each of the plans that are available, you can also register users, after the records of the plans and users exist, you can perform the registrations of the users with the chosen plan, when doing this process the system will make a calculation of the total to be paid, the start and end date of the registration, you can also view a list of prices, users in the system.

## Installation
Load project in a local environment, do the following:

Download project from this repository:

**Preparing the Backend:**
1. Create a project in [firebase](https://firebase.google.com/), The application is web, follow the steps for its correct installation.
2. When getting the credentials of your firebase project, you will need to add your credentials in the "firebase.service.ts" file in the "firebaseConfig" constant.
3. Enable authentication (by mail and password) is used to log in.
4. Enable Storage and add a folder called "clientes", this is used to store the images.
5. Enable Firebase Database and add three documents:
```
    1. "clientes", the id is automatic, then the fields must be added: apellido (string), cedula (string), correo (string), fechaNacimiento (string), imgUrl (string), nombre (string), telefono (number).
    2. "inscripciones", the id is automatic, then the fields must be added: cliente (reference), fecha (timestamp), fechaFinal (timestamp), isv (number), precios (reference), subTotal (number), total (number).
    3. "precios", the id is automatic, then the fields must be added: costo (number), duracion (number), nombre (string), tipoDuracion (string).
```

**Preparing the FronEnd:**
1. Install [nodejs](https://nodejs.org/es/), I recommend the LTS version which is more stable.
2. Install [AngularCLI](https://angular.io/guide/setup-local).

At the end continue with:
```
npm install
ng serve
```
ready...

More components that were used:
[ComponenteNGXBootstrap](https://valor-software.com/ngx-bootstrap/#/components), we will be able to execute bootstrap component scripts.
[NGXSpinner](https://www.npmjs.com/package/ngx-spinner), it is used to show the loading panel, with a friendlier design.

## Support
franciscosobrevilla97@gmail.com

## Usage
When loading the project for the first time, you will need to have a user registered from the Firebase database, since the application does not currently have a screen that shows the creation of gym administrator users.

## Project status
Suspended
